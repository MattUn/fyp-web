import React from "react";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "./icons.js";
import ViewerSearch from "./screens/ViewerSearch";
import ViewerMain from "./screens/ViewerMain";
import "./style.css";
import Designer from "./screens/Designer";
import Help from "./screens/Help";
import ErrorPage from "./screens/ErrorPage";
import NewDesigner from "./screens/NewDesigner";

function App() {
//maps all urls to components including passing url parameters
  return (
    <Router>
        <Route path="/help" exact component={Help} />
        <Route path="/error/:component/:config" exact component={ErrorPage} />
        <Route path="/viewer/:config" exact component={ViewerSearch} />
        <Route path="/viewer/search/:config/:query" exact component={ViewerMain} />
        <Route path="/designer/:config/:access_code" exact component={Designer} />
        <Route path="/designer" exact component={NewDesigner} />
    </Router>
  );
}

export default App;
