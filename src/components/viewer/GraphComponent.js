import React, { useState } from "react";
import Graph from "react-graph-vis";
import styled from "styled-components";

let added = false;
function RelationGraph1() {
    const [graph, setGraph] = useState({
        nodes: [
            {
                id: 99999,
                label: "LOADING - please wait"//using a node to initialise the chart and dual purpose as loading indicator
            }
        ],
        edges: [],

    });

    const [options, setOptions] = useState( {
        layout: {
            hierarchical: false
        },
        edges: {
            color: "#1D1D1D"
        },
        interaction: {
            hover: false,
            navigationButtons: false,
            tooltipDelay: 0
        },
        groups: {}
    });

    const events = {
        startStabilizing: () => {
            (async() => {
                while(typeof propsGlobal.graphData == 'undefined' || typeof propsGlobal.graphData.nodes == 'undefined') {
                    await new Promise(resolve => setTimeout(resolve, 500));//will check every 0.5s
                }
                addData(propsGlobal.graphData);
            })();
        },
        select: function (event) {
            let { nodes} = event;
            propsGlobal.nodeSelectHandler(nodes);
        },
    };

    return (
        <Graph
            graph={graph}
            options={options}
            events={events}
        />
    );


    function addData(data){
        if(added){return}//stops this function running multiple times
        added = true;
        //groups
        let new_groups = {useDefaultGroups: true}
        for (let i=0; i < data.types.length; i++){
            let type = data.types[i];
            let grp ={shape: type.shape,color:{background:type.colour} }
            new_groups[type.name] = grp;
        }
        const tmp_opt = { ...options };
        tmp_opt.groups = new_groups;
        setOptions(tmp_opt);


        //nodes and edges
        let new_nodes = [];
        let new_edges = [];
        for (let i=0; i < data.nodes.length; i++){
            let node = data.nodes[i];
            let graph_node = {id: node.node_id, label: node.title, title: node.title, group: node.type}
            new_nodes.push(graph_node);
            if (node.node_id_source !== node.node_id){
                let edge_node = { from: node.node_id_source, to: node.node_id }
                new_edges.push(edge_node);
            }
        }

        const tmp_graph = { ...graph };
        tmp_graph.nodes = new_nodes;
        tmp_graph.edges = new_edges;
        setGraph(tmp_graph);
    }
}

let propsGlobal;
export default function GraphComponent(props) {
    propsGlobal=props;
    return (
        <Container className="GC3">
            <RelationGraph1 />
        </Container>
    );
}

const Container = styled.div`
  display: flex;
  flex-shrink: 0;
  flex-direction: column;
  height: 75vh;
  max-height: 80vh;
  width: 50vw;
  min-width: 50vw;
  max-width: 50vw
`;
