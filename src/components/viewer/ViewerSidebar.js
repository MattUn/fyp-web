import React from "react";
import styled from "styled-components";
import fitty from "fitty";

function ViewerSidebar(props) {
    fitty('#logo');//used to keep logo within space
    require('dotenv').config()
    //map is to replace newline chars with html brs
  return (
    <Container {...props}>
      <BrowseInOne id="logo">BrowseInOne</BrowseInOne>
      <WalkingPlanner>{props.state.name}</WalkingPlanner>
      <DescriptionText>
          {props.state.description.split('\n').map(function(item, i) {
              return (
                  <span key={i}>
                      {item}
                      <br/>
                  </span>
              )
          })}
      </DescriptionText>
      <HelpText>
        For details on how to use BrowseInOne <a href="/help">click here</a>
      </HelpText>
    </Container>
  );
}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 20vw;
  max-width: 20vw;
  height: 100vh;
  background-color: #E6E6E6;
`;

const BrowseInOne = styled.span`
  font-family: Share Tech,monospace;
  position: relative;
  top: 10px;
  font-style: normal;
  font-weight: 700;
  color: #121212;
  font-size: 44px;
  text-align: center;
  max-width: 20vw;
`;

const WalkingPlanner = styled.span`
  font-family: Roboto,sans-serif;
  position: relative;
  top: 30px;
  margin-left: 10%;
  margin-right: 10%;
  font-style: normal;
  font-weight: 500;
  color: #121212;
  font-size: 28px;
  text-align: center;
  max-width: 18vw;
`;

const DescriptionText = styled.span`
  font-family: Roboto,sans-serif;
  position: relative;
  top: 3em;
  margin-left: 5%;
  margin-right: 5%;
  font-style: normal;
  font-weight: 400;
  color: #121212;
  font-size: 14px;
  text-align: left;
  max-width: 20vw;
`;

const HelpText = styled.span`
  font-family: Roboto,sans-serif;
  position: relative;
  top: 8em;
  margin-left: 5%;
  margin-right: 5%;
  font-style: normal;
  font-weight: 400;
  color: #121212;
  font-size: 14px;
  text-align: left;
  max-width: 20vw;
`;

export default ViewerSidebar;
