import React, {useState} from "react";
import Graph from "react-graph-vis";
import styled from "styled-components";

let added = false;
function RelationGraph1() {//just a graph like the larger one but types only
    const [graph, setGraph] = useState({
        nodes: [],
        edges: [],

    });

    const [options] = useState( {
        layout: {
            hierarchical: false
        },
        edges: {
            color: "#1D1D1D"
        },
        interaction: {
            hover: false,
            navigationButtons: false,
            tooltipDelay: 0
        }
    });

    const events = {
        startStabilizing: () => {
            (async() => {
                while(typeof propsGlobal.graphData == 'undefined' || typeof propsGlobal.graphData.nodes == 'undefined') {
                    await new Promise(resolve => setTimeout(resolve, 500));
                }
                addData(propsGlobal.graphData);
            })();
        }
    };

    return (
        <Graph
            graph={graph}
            options={options}
            events={events}
        />
    );


    function addData(data){
        if(added){return}//stops this function running multiple times
        added = true;

        let new_nodes = [];
        for (let i=0; i < data.types.length; i++){
            let type = data.types[i];
            let leg_node={
                id: i,
                label: type.name,
                shape: type.shape,
                color:{background: type.colour},
                fixed: {x:false, y:false},
                x: 900/(data.types.length+1)*(i-0.5),
                y: 0,
            }
            new_nodes.push(leg_node);
        }

        const tmp_graph = { ...graph };
        tmp_graph.nodes = new_nodes;
        setGraph(tmp_graph);
    }
}

let propsGlobal;
export default function GraphLegend(props) {
    propsGlobal=props;
    return (
        <Container className="GraphLegend">
            <RelationGraph1 />
        </Container>
    );
}

const Container = styled.div`
  display: flex;
  flex-shrink: 0;
  flex-direction: column;
  height: 20vh;
  max-height: 13vh;
  width: 50vw;
  min-width: 50vw;
  max-width: 50vw
`;

