import React, {Component} from "react";
import styled from "styled-components";


export default class Des4Output extends Component{
    render(){
        let viewerUrlSub = '/viewer/'.concat(this.props.state.config_code);
        let designerUrlSub = '/designer/'.concat(this.props.state.config_code).concat('/'.concat(this.props.state.access_code));
        return (
            <Container>
                <SaveBtn onClick={this.props.doSave}>
                    <b>SAVE</b>
                </SaveBtn>
                <p>Viewer</p>
                <p><a href={viewerUrlSub} target="_blank" rel="noopener noreferrer">{viewerUrlSub}</a></p>
                <hr/>
                <p>Designer</p>
                <p><a href={designerUrlSub} target="_blank" rel="noopener noreferrer">{designerUrlSub}</a></p>
            </Container>
        );
    }


}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 94vw;
  height: 86vh;
  background-color: #FFFFFF;
  margin-left: 1vw;
  margin-right: 1vw;
`;

const SaveBtn = styled.button`
  width: 90vw;
  height: 10vh;
  background-color: #DDDDDD;
  margin-left: 1vw;
  margin-right: 1vw;
`;