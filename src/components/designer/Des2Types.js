import React, {Component} from "react";
import styled from "styled-components";
import Dropdown from "react-dropdown";
import 'react-dropdown/style.css';


export default class Des2Types extends Component{

    constructor(props) {
        super(props);
        this.state ={
            ...this.props.state,
            tableInsertionPoints: <tr><td> </td><td> </td></tr>,
            tableValues: <tr><td> </td><td> </td></tr>,
            typeLists: [''],
            modifyingType: Object.keys(this.props.state.type)[0]
        }
    }

    componentDidMount() {
        this.init_type_list();
    }

    init_type_list(){
        let list_items = [];
        for (const [key, value] of Object.entries(this.props.state.type)) {
            list_items.push({value: key, label: key.concat(' - ').concat(value.name) });
        }
        this.setState(() => ({
            typeLists: list_items,
            modifyingType: list_items[0].value
        }))
    }

    onTextChange = (event) => {
        let event_name = event.target.name;
        let event_value = event.target.value;
        let type_obj = this.props.state.type;
        type_obj[this.state.modifyingType][event_name] = event_value;
        let state_dict = {'type': type_obj};
        this.props.stateUpdate(state_dict);

        //update type dropdown
        if (event_name==='name'){
            let typeLists = this.state.typeLists;
            for (const item of typeLists){
                if (item.value===this.state.modifyingType){
                    item.label = this.state.modifyingType.concat(' - ').concat(event_value);
                }
            }
            this.setState(() => ({
                typeLists: typeLists,
            }))
        }
    }

    rerender = '';
    onTypeChange = (event) => {
        this.setState(() => ({
            modifyingType: event.value
        }))
        this.doInsertPointTable(event.value);
        this.doValuesTable(event.value);
        this.rerender = event.value;
    }

    onCheckbox = (event) => {
        let type_obj = this.props.state.type;
        type_obj[this.state.modifyingType].is_start = event.target.checked;
        let state_dict = {'type': type_obj};
        this.props.stateUpdate(state_dict);
    }

    onTestValChange = (event) => {//run on insertion point test value change
        let state_insertions = this.props.state.type[this.state.modifyingType].insertions;
        state_insertions[event.target.name] = event.target.value;

        let type_obj = this.props.state.type;
        type_obj[this.state.modifyingType].insertions = state_insertions;
        let state_dict = {'type': type_obj};
        this.props.stateUpdate(state_dict);
    }

    currentTestVal = (point_name, modType = this.state.modifyingType) => {
        let state_insertions = this.props.state.type[modType].insertions;
        return state_insertions[point_name];
    }

    doInsertionPointLoad = () => {//voids all previous and creates new
        let url = this.props.state.type[this.state.modifyingType].url;
        let insertionPoints = url.match(/{(\w+)}/g) || [];
        let new_insertions = {};
        for (const point of insertionPoints){
            new_insertions[point] = '';
        }
        let type_obj = this.props.state.type;
        type_obj[this.state.modifyingType].insertions = new_insertions;
        let state_dict = {'type': type_obj};
        this.props.stateUpdate(state_dict);

        this.doInsertPointTable();
    }

    doInsertPointTable = (modType = this.state.modifyingType) => {
        let key =Date.now();
        this.setState(() => ({
            tableInsertionPoints: (<tbody key={key}>
            {Object.keys(this.props.state.type[modType].insertions).map((point_name) => {
                if (point_name==0){
                    return ('');
                }
                return (
                    <tr><td>{point_name}</td><td><input name={point_name}
                                                        type="text"
                                                        defaultValue={this.currentTestVal(point_name, modType)}
                                                        onBlur={this.onTestValChange}/></td></tr>
                )
            })}
            </tbody>),
        }))
        this.rerender = Date.now();
    }


    onDisplayValChange = (event) => {//run on insertion point test value change
        let state_returns = this.props.state.type[this.state.modifyingType].returns;
        state_returns[event.target.name] = event.target.value;

        let type_obj = this.props.state.type;
        type_obj[this.state.modifyingType].returns = state_returns;
        let state_dict = {'type': type_obj};
        this.props.stateUpdate(state_dict);
    }

    currentDisplayVal = (field_name, modType = this.state.modifyingType) => {
        let state_returns = this.props.state.type[modType].returns;
        return state_returns[field_name];
    }

    doValuesLoad = () => {//voids all previous and creates new. For second table
        let url = this.props.state.type[this.state.modifyingType].url;
        let state_insertions = this.props.state.type[this.state.modifyingType].insertions;

        let dct_send = {insertions: state_insertions, url: url};

        require('dotenv').config()
        fetch(process.env.REACT_APP_SERVER_URL+'designer/'.concat(this.state.config_code).concat('/').concat(this.state.access_code).concat('/test'),
            {
                method: 'POST',
                headers: {
                    accept: 'application/json',
                },
                body: JSON.stringify(dct_send)
            })
            .then(response => {
                if(response.ok) {
                    return response;
                } else {
                    alert('invalid URL or parameters provided');
                    return '{}';
                }
            })
            .then((response) => response.json())
            .then((result) => {
                let new_fields = {};
                for (const field of Object.keys(result)){
                    new_fields[field] = '';
                }
                let type_obj = this.props.state.type;
                type_obj[this.state.modifyingType].returns = new_fields;
                let state_dict = {'type': type_obj};
                this.props.stateUpdate(state_dict);

                this.doValuesTable();
            })
    }

    doValuesTable = (modType = this.state.modifyingType) => {
        let key =Date.now();
        this.setState(() => ({
            tableValues: (<tbody key={key}>
            {Object.keys(this.props.state.type[modType].returns).map((field_name) => {
                if (field_name==0){
                    return ('');
                }
                return (
                    <tr><td>{field_name}</td><td><input name={field_name}
                                                        type="text"
                                                        defaultValue={this.currentDisplayVal(field_name, modType)}
                                                        onBlur={this.onDisplayValChange}/></td></tr>
                )
            })}
            </tbody>),
        }))
    }


    render(){
        return (
            <Container key={this.rerender}>
                <p>Type to modify</p>
                <Dropdown options={this.state.typeLists} value={this.state.modifyingType} onChange={this.onTypeChange}/>{/*setting initial value ensures value never null*/}
                <HorizontalContainer>
                    <InsideStack>
                        <p>Type name</p>
                        <input
                            name="name"
                            type="text"
                            defaultValue={this.props.state.type[this.state.modifyingType].name}
                            onBlur={this.onTextChange}
                        />
                    </InsideStack>
                    <InsideStack>
                        <p>Attribution</p>
                        <input
                            name="attribution"
                            type="text"
                            placeholder="The name of the data source provider"
                            defaultValue={this.props.state.type[this.state.modifyingType].attribution}
                            onBlur={this.onTextChange}
                        />
                    </InsideStack>
                </HorizontalContainer>
                <p>Is this the starting node?</p>
                <input
                    type="checkbox"
                    checked={this.props.state.type[this.state.modifyingType].is_start}
                    onChange={this.onCheckbox}
                />
                <p>URL - including insertion points</p>
                <input
                    name="url"
                    type="text"
                    defaultValue={this.props.state.type[this.state.modifyingType].url}
                    onBlur={this.onTextChange}
                />
                <br/>
                <Btn onClick={this.doInsertionPointLoad}>
                    Load insertion points
                </Btn>
                <TableScroller>
                    <table>
                        <thead><tr><TableHeader>Insertion Points</TableHeader><TableHeader>Valid test values</TableHeader></tr></thead>
                        {this.state.tableInsertionPoints}
                    </table>
                </TableScroller>

                <br/>
                <Btn onClick={this.doValuesLoad}>
                    Test with values
                </Btn>
                <TableScroller>
                    <table>
                        <thead><tr><TableHeader>Fields returned from API</TableHeader><TableHeader>Display name</TableHeader></tr></thead>
                        {this.state.tableValues}
                    </table>
                </TableScroller>
            </Container>
        );
    }


}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 94vw;
  height: 86vh;
  background-color: #FFFFFF;
  margin-left: 1vw;
  margin-right: 1vw;
`;

const HorizontalContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 94vw;
  min-width: 94vw;
  height: 10vh;
  background-color: #FFFFFF;
  margin-left: 1vw;
  margin-right: 1vw;
`;

const InsideStack = styled.div`
  display: flex;
  flex-direction: column;
  width: 40vw;
  height: 10vh;
  background-color: #FFFFFF;
  margin-left: 1vw;
  margin-right: 1vw;
`;

const Btn = styled.button`
  width: 20vw;
  height: 5vh;
  background-color: #DDDDDD;
  margin-left: 1vw;
  margin-right: 1vw;
`;

const TableHeader = styled.th`
  background: white;
  position: sticky;
  top: 0;
  height: 3vh;
  max-height: 3vh;
  min-width: 40em;
`;

const TableScroller = styled.span`
  height: 15vh;
  max-height: 23vh;
  overflow-y: auto;
`;