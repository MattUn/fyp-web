import React, {Component} from "react";
import styled from "styled-components";
export default class Des1Summary extends Component{

    constructor(props) {
        super(props);
        this.state = props.state;
    }

    convertBase64 = (file) => {
        return new Promise((resolve, reject) => {
            const fileReader = new FileReader();
            fileReader.readAsDataURL(file)
            fileReader.onload = () => {
                resolve(fileReader.result);
            }
            fileReader.onerror = (error) => {
                reject(error);
            }
        })
    }

    render(){
        let onTextChange = (event) => {
            let state_dict = {[event.target.name]: event.target.value};
            this.props.stateUpdate(state_dict);
        }

        let onImageUpload = async (event) => {
            let img_file = event.target.files[0];
            let name = event.target.name;
            const img_b64 = await this.convertBase64(img_file);
            let state_dict = {[name]: img_b64};
            this.props.stateUpdate(state_dict);
        }
        return (
            <Container>
                <p>Mashup name</p>
                <input
                    name="summary_name"
                    type="text"
                    defaultValue={this.props.state.summary_name}
                    onBlur={onTextChange}
                />
                <p>Description</p>
                <TextArea
                    name="summary_description"
                    defaultValue={this.props.state.summary_description}
                    onBlur={onTextChange}
                />
                <p>Background image</p>
                <input
                    name="summary_image"
                    type="file"
                    id="summary_image"
                    accept="image/*"
                    onChange={onImageUpload}
                />
                <p>User input prompt</p>
                <input
                    name="start_prompt"
                    type="text"
                    defaultValue={this.props.state.start_prompt}
                    onBlur={onTextChange}
                />
            </Container>
        );
    }


}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 94vw;
  height: 86vh;
  background-color: #FFFFFF;
  margin-left: 1vw;
  margin-right: 1vw;
`;

const TextArea = styled.textarea`
  height: 20vh;
  font-family: sans-serif;
`;

