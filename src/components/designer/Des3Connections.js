import React, {Component} from "react";
import styled from "styled-components";
import Dropdown from "react-dropdown";
import 'react-dropdown/style.css';


export default class Des3Connections extends Component{

    constructor(props) {
        super(props);
        this.state ={
            ...this.props.state,
            tableBody: <tr><td> </td><td> </td></tr>,
            typeListStartOptions: [''],
            typeListEndOptions: [''],
            typeListStartCurrent: '■',
            typeListEndCurrent: '●',
            varOptions: [],
        }
    }

    componentDidMount() {

        this.set_type_lists();
    }

    set_type_lists(){
        let list_items_start = [];
        let list_items_end = [];
        for (const [key, value] of Object.entries(this.props.state.type)) {
            if(value.name.length>0){//show labels as shape and text if it has it
                list_items_start.push({value: key, label: key.concat(' - ').concat(value.name) });
                if(!value.is_start){//start items cannot go in the end list
                    list_items_end.push({value: key, label: key.concat(' - ').concat(value.name) });
                }
            }
        }
        if (list_items_start.length>=2){
            this.setState(() => ({
                typeListStartOptions: list_items_start,
                typeListEndOptions: list_items_end,
                typeListStartCurrent: list_items_start[0].value,
                typeListEndCurrent: list_items_end[0].value,
            }))
        }

    }


    rerender = '';//changing this key will make react think a component has changed so it will rerender
    onStartTypeChange = (event) => {
        this.setState(() => ({
            typeListStartCurrent: event.value,
            varOptions: Object.values(this.props.state.type[this.state.typeListStartCurrent].returns).filter(e => e),
        }))
        this.doTable(event.value,undefined);
        this.rerender = event.value;
        (async() => {
            while(this.state.varOptions.length===0) {
                await new Promise(resolve => setTimeout(resolve, 100));
            }
            this.rerender = Date.now();
        })();
    }
    onEndTypeChange = (event) => {
        this.setState(() => ({
            typeListEndCurrent: event.value
        }))
        this.doTable(undefined,event.value);
        this.rerender = event.value;
    }


    onVarChange = (event) => {//run on var selection change
        let start = this.state.typeListStartCurrent;
        let end = this.state.typeListEndCurrent;
        let conn_obj = this.props.state.conn;

        let inside = {};
        inside[event.value] = event.label;

        if(end in conn_obj){
            if(start in conn_obj[end]){
                conn_obj[end][start] = {...conn_obj[end][start],...inside};
            }else{
                let obj ={};
                obj[start] = inside;
                conn_obj[end] = obj;
            }
        }else{
            let obj = {};
            let objInner = {};
            objInner[start] = inside;
            obj[end] = objInner;
            conn_obj = obj;
        }

        let state_dict = {'conn': conn_obj};
        this.props.stateUpdate(state_dict);
    }


    doTable = (start = this.state.typeListStartCurrent, end = this.state.typeListEndCurrent) => {
        let key =Date.now();

        this.setState(() => ({
            tableBody: (<tbody key={key}>
            {Object.keys(this.props.state.type[end].insertions).map((point_name) => {
                if (point_name==0){
                    return ('');
                }
                let newOptions= [];
                for (const option of this.state.varOptions){
                    newOptions.push({label: option,value: point_name})
                }
                return (
                    <tr><td>{point_name}</td><td><Dropdown className={point_name}
                        options={newOptions}
                        onChange={this.onVarChange}
                        key ={Date.now()}
                        /></td></tr>
                )
            })}
            </tbody>),
        }))
    }


    render(){
        return (
            <Container >
                <p>Start type of link</p>
                <Dropdown options={this.state.typeListStartOptions} value={this.state.typeListStartCurrent} onChange={this.onStartTypeChange}/>
                <p>End type of link</p>
                <Dropdown options={this.state.typeListEndOptions} value={this.state.typeListEndCurrent} onChange={this.onEndTypeChange}/>
                <br/>

                <TableScroller>
                    <table key={this.rerender}>
                        <thead><tr><TableHeader>Insertion Points</TableHeader><TableHeader>Variables to insert</TableHeader></tr></thead>
                        {this.state.tableBody}
                    </table>
                </TableScroller>
            </Container>
        );
    }


}

const Container = styled.div`
  display: flex;
  flex-direction: column;
  width: 94vw;
  height: 86vh;
  background-color: #FFFFFF;
  margin-left: 1vw;
  margin-right: 1vw;
`;

const TableHeader = styled.th`
  background: white;
  position: sticky;
  top: 0;
  height: 3vh;
  max-height: 3vh;
  min-width: 40em;
`;

const TableScroller = styled.span`
  height: 40vh;
  max-height: 40vh;
  overflow-y: auto;
`;