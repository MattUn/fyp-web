import React, {Component} from "react";
import styled from "styled-components";
import {Tab, TabList, TabPanel, Tabs} from "react-tabs";//https://reactcommunity.org/react-tabs/
import 'react-tabs/style/react-tabs.css';

import Des1Summary from "../components/designer/Des1Summary";
import Des2Types from "../components/designer/Des2Types";
import Des3Connections from "../components/designer/Des3Connections";
import Des4Output from "../components/designer/Des4Output";

export default class Designer extends Component{
    constructor(props) {
        super(props);
        let config=props.match.params.config;
        let access_code=props.match.params.access_code;
        this.state = {
            //this is the core state for the whole designer
            config_code: config,
            access_code: access_code,

            summary_name: '',
            summary_description: '',
            summary_image: '',
            start_prompt: '',

            type: default_types,

            conn: {},

        }
    }

    stateUpdate = (state_dict) => {
        this.setState((prevState) => ({
            ...prevState,
            ...state_dict
        }))
    }


    componentDidMount() {
        this.loadState();
    }

    render() {
        return (
            <Container>
                <BrowseInOne>BrowseInOne Designer</BrowseInOne>
                <Tabs>
                    <TabList>
                        <Tab>1. Summary</Tab>
                        <Tab>2. Types</Tab>
                        <Tab>3. Connections</Tab>
                        <Tab>4. Output</Tab>
                    </TabList>
                    <TabPanel><Des1Summary state={this.state} stateUpdate={this.stateUpdate} /></TabPanel>
                    <TabPanel><Des2Types state={this.state} stateUpdate={this.stateUpdate}/></TabPanel>
                    <TabPanel><Des3Connections state={this.state} stateUpdate={this.stateUpdate}/></TabPanel>
                    <TabPanel><Des4Output state={this.state} stateUpdate={this.stateUpdate} doSave={this.saveState}/></TabPanel>
                </Tabs>
            </Container>
        );
    }

    loadState() {//send to server
        require('dotenv').config()
        fetch(process.env.REACT_APP_SERVER_URL+'designer/'.concat(this.state.config_code).concat('/').concat(this.state.access_code).concat('/load'),
            {
                method: 'GET',
                headers: {
                    accept: 'application/json',
                },
            })
            .then(response => {
                if(response.ok) {
                    return response;
                } else {
                    throw Error(`Request rejected: status code ${response.status}`);
                }
            })
            .then((response) => response.json())
            .then((result) => {
                let type={...default_types,...result['type']}
                result.type=type;
                this.setState(result);
            })
    }

    saveState = () => {//get from server
        fetch(process.env.REACT_APP_SERVER_URL+'designer/'.concat(this.state.config_code).concat('/').concat(this.state.access_code).concat('/save'),
            {
                method: 'POST',
                headers: {
                    accept: 'application/json',
                },
                body: JSON.stringify(this.state)
            }).then(response => {
            if(response.ok) {
                alert('Saved. View at links below.');
            } else {
                alert('Could not save. '.concat(response.statusText))
            }
        })

    }
}

const Container = styled.div`
  display: flex;
  flex-shrink: 0;
  flex-direction: column;
  height: 100vh;
  width: 100vw;
`;

const BrowseInOne = styled.span`
  font-family: Share Tech,monospace;
  position: relative;
  top: 1vh;
  font-style: normal;
  font-weight: 700;
  color: #121212;
  font-size: 36px;
  text-align: center;
`;

const default_types = {
    '■': {
        name: '',
        attribution: '',
        url: '',
        is_start: false,
        insertions: {},
        returns: {},
    },
    '●': {
        name: '',
        attribution: '',
        url: '',
        is_start: false,
        insertions: {},
        returns: {},
    },
    '◆': {
        name: '',
        attribution: '',
        url: '',
        is_start: false,
        insertions: {},
        returns: {},
    },
    '▲': {
        name: '',
        attribution: '',
        url: '',
        is_start: false,
        insertions: {},
        returns: {},
    },
    '▼': {
        name: '',
        attribution: '',
        url: '',
        is_start: false,
        insertions: {},
        returns: {},
    },
}