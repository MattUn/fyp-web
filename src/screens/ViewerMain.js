import React, {Component} from "react";
import styled from "styled-components";
import ViewerSidebar from "../components/viewer/ViewerSidebar";
import GraphComponent from "../components/viewer/GraphComponent";
import GraphLegend from "../components/viewer/GraphLegend";
import {Redirect} from "react-router-dom";

export default class ViewerMain extends Component{
    constructor(props) {
        super(props);
        let config=props.match.params.config;
        let query=props.match.params.query;
        this.init(config, query);
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }



    state = {
        config_id: '',
        name: '',
        description: '',
        question: '',
        image: '',
        width: 0,
        height: 0,
        query: '',

        point_type: '',
        point_title: '',
        point_description: '',
        point_attrib: '',
        data: {},

    }

    init = (config_id, query_string) => {
        ServerFetchGraph(config_id,query_string)
            .catch(() => {this.setState({config_id: config_id, data: {invalid: true}})})
            .then((result) => {
            this.setState(() => ({
                data: result
            }))
        });

        ServerFetchStart(config_id)
            .catch(() => {this.setState({config_id: config_id, data: {invalid: true}})})
            .then((result) => {
            this.setState(() => ({
                name: result['name'],
                description: result['description'],
                question: result['question'],
                image: result['image']
            }))
        });

    }

    handleNodeSelect = (e) =>{//passed to graph so that it can update the state here
        if(e.length>0) {
            if(e[0]!==99999) {
                this.setState(() => ({
                    point_type: this.state.data.nodes[e].type,
                    point_title: this.state.data.nodes[e].title,
                    point_description: this.state.data.nodes[e].description,
                    point_attrib: this.state.data.nodes[e].attribution,
                }))
            }
        }else{
            this.setState(() => ({
                point_type: '',
                point_title: '',
                point_description: '',
                point_attrib: '',
            }))
        }
    }

    render(){
        //<GraphComponent nodeSelectHandler={this.handleNodeSelect} graphData = {this.state.data}/>
        if (this.state.data['invalid']) {
            return <Redirect to={'/error/viewer/'.concat(this.state.config_id)}/>
        }
        return (
            <Container>
                <ViewerSidebar state={this.state}/>
                <CentreStack>
                    <GraphComponent nodeSelectHandler={this.handleNodeSelect} graphData = {this.state.data}/>
                    <hr/>
                    <p><b>Legend</b></p>
                    <GraphLegend graphData = {this.state.data}/>
                </CentreStack>
                <DetailsSidebar>
                    <PanelTitle>Details of currently selected point</PanelTitle>
                    <SelectionTitle>{this.state.point_type} : {this.state.point_title}</SelectionTitle>
                    <DescriptionText>{this.state.point_description.split('\n').map(function(item) {
                        return (
                        <span>
                            {item}
                            <br/>
                        </span>
                        )
                    })}</DescriptionText>
                    <DataSource>Source: {this.state.point_attrib}</DataSource>
                </DetailsSidebar>
            </Container>
        );
    }
}

function ServerFetchStart(config_code) {
    require('dotenv').config()
    return fetch(process.env.REACT_APP_SERVER_URL+'start/'+config_code,
        {
            method: 'GET',
            headers: {
                accept: 'application/json',
            },
        })
        .then(response => {
            if(response.ok) {
                return response;
            } else {
                throw Error(`Request rejected: status code ${response.status}`);
            }
        })
        .then((response) => response.json())
}

function ServerFetchGraph(config_code, query){
    require('dotenv').config()
    return fetch(process.env.REACT_APP_SERVER_URL+'graph/'+config_code+'/'+query,
        {
            method: 'GET',
            headers: {
                accept: 'application/json',
            },
        })
        .then(response => {
            if(response.ok) {
                return response;
            } else {
                throw  Error(`Request rejected: status code ${response.status}`);
            }
        })
        .then((response) => response.json())
}


const Container = styled.div`
  display: flex;
  flex-shrink: 0;
  flex-direction: row;
  height: 100vh;
  width: 100vw;
`;

const CentreStack = styled.div`
  display: flex;
  flex-shrink: 0;
  flex-direction: column;
  height: 100vh;
  width: 50vw;
  min-width: 50vw;
  max-width: 50vw
`;

const DetailsSidebar = styled.div`
  display: flex;
  flex-direction: column;
  min-width: 30vw;
  max-width: 30vw;
  height: 100vh;
  background-color: #E6E6E6;
`;

const PanelTitle = styled.span`
  font-family: Roboto,sans-serif;
  position: relative;
  top: 5px;
  margin-left: 10%;
  margin-right: 10%;
  font-style: normal;
  font-weight: 500;
  color: #121212;
  font-size: 28px;
  text-align: center;
`;

const SelectionTitle = styled.span`
  font-family: Roboto,sans-serif;
  position: relative;
  top: 15px;
  margin-left: 10%;
  margin-right: 10%;
  font-style: normal;
  font-weight: 400;
  color: #121212;
  font-size: 20px;
  text-align: center;
`;

const DescriptionText = styled.span`
  font-family: Roboto,sans-serif;
  position: relative;
  top: 3em;
  min-height: 76vh;
  height: 76vh;
  max-height: 76vh;
  overflow-y: scroll;
  margin-left: 5%;
  margin-right: 5%;
  font-style: normal;
  font-weight: 400;
  color: #121212;
  font-size: 14px;
  text-align: left;
`;

const DataSource = styled.span`
  font-family: Roboto,sans-serif;
  position: relative;
  top: 4em;
  margin-left: 5%;
  margin-right: 5%;
  font-style: normal;
  font-weight: 400;
  color: #121212;
  font-size: 14px;
  text-align: left;
`;
