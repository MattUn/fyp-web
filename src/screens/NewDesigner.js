import React, {Component} from "react";
import {Redirect} from "react-router-dom";

export default class NewDesigner extends Component{

    state = {
        config_code: '',
        access_code: ''
    }

    componentDidMount() {
        ServerFetchNew()
            .catch(() => {this.setState({config_code: 'invalid'})})
            .then((result) => {
                this.setState(() => ({
                    config_code: result.config_code,
                    access_code: result.access_code
                }))
            });
    }



    render(){
        if (this.state.config_code.length===8) {//redirect as soon as new data from server
            return <Redirect to={'/designer/'.concat(this.state.config_code).concat('/').concat(this.state.access_code)}/>
        }else if (this.state.config_code==='invalid'){//redirect to error page if server fails
            return <Redirect to={'error/designer/invalid'}/>
        }

        return(
            <div>
                <h2>BrowseInOne</h2>
                <h1>Loading</h1>
            </div>
        )
    }

}

function ServerFetchNew() {
    require('dotenv').config()
    return fetch(process.env.REACT_APP_SERVER_URL + 'make_new',
        {
            method: 'GET',
            headers: {
                accept: 'application/json',
            },
        })
        .then(response => {
            if (response.ok) {
                return response;
            } else {
                throw  Error(`Request rejected: status code ${response.status}`);
            }
        })
        .then((response) => response.json());
}