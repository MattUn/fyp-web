import React, {Component} from "react";
import styled from "styled-components";

export default class Help extends Component{
    render(){
        return(
            <Container>
                <h1>BrowseInOne help</h1>
                <p>BrowseInOne is an application for configurable web service integration and visualisation 'mashups'.
                This is achieved through two parts of the application, the designer for configuring how the web
                services composition will be created, and the viewer to view already created compositions
                <br/>Web services are resources on the web that are accessible for machines and are not intended for direct human consumption</p>
                <br/><br/>
                <h1>Viewer</h1>
                <p>To start with you will see the graph taking up the majority of the screen. If loading shows for
                a continued period then you should refresh the page.</p>
                <p>The graph and contents can be repositioned by dragging the background, zoomed with the scroll wheel,
                and nodes can be repositioned by dragging on them.</p>
                <p>Clicking any node will show all details for it in the right hand sidebar. Clicking on the background
                will hide the details.</p>
                <br/><br/>
                <h1>Designer</h1>
                <p>The designer is at the core of the application as it is the start point for all compositions.</p>
                <p>It is split into 4 tabs which as far as practical should be followed in numeric order.</p>
                <p>Visiting /designer will create a new composition whereas accessing an existing designer URL will load saved values back.</p>

                <h3>Summary tab</h3>
                <p>The summary page contains simple options for things that will get directly shown to the user.</p>
                <p>Mashup name and Description and shown to the user in the left sidebar throughout the viewer application.</p>
                <p>The background image is shown as the background on the main search page and the user input prompt is the question above the search box.</p>

                <h3>Types  tab</h3>
                <p>There are 5 predefined shapes for the maximum 5 types that you are allowed to use. Types without a title are ignored.
                The type you select in the 'Type to modify' dropdown is what the rest of the page will relate to</p>
                    <p>Type names are what will be shown in the legend at at the top of the info panel whereas the attribution (the data source provider)
                    goes at the bottom of the info panel</p>
                    <p>If selecting a node is a starting node, please be aware that it can then only have one insertion point and that will be
                    the user's search query.</p>
                    <p>When entering a URL, anything you want to be replaced with other values must be plain alphanumeric characters enclosed in curly braces.
                        For example, if you wanted to get http://data.com/api/Leeds or http://data.com/api/Bradford with a location variable that in
                        these cases was Leeds or Bradford respectively, you'd enter http://data.com/api/&#123;location&#125; and use a variable called location.
                    Once this is put in the input box, press 'Load insertion points' to populate the first table. From there you can fill in the
                    'Valid test values' column which will insert those values into the corresponding insertion points so use values that will cause a successful
                    response.</p>
                <p>Following that press the 'Test with values' button which will populate the second table with all the fields returned from the API.
                For any that you want to use you must enter a display name which it will be known as from then on. To choose one as the Type title, it must
                be named 'TITLE'</p>

                <h3>Connections tab</h3>
                <p>The connections tab requires Types to have already been set up in order to use them so while you can go back and make some changes
                to Types after making connections, it is recommended to minimise this.</p>
                <p>The top pair of dropdowns are for selecting which link/connection you want to configure.
                    The arrows and data flow is from the start type to end type.</p>
                <p>Be aware that start points can't be selected as end types.</p>
                <p>When a valid pair is selected the insertion points of the end type will fill in the table below and dropdowns containing
                values from the selected start type appear next to them. It is then just a case of matching the dropdown value to the insertion point.</p>

                <h3>Output tab</h3>
                <p>The output tab contains links to the viewer and designer pages for this composition and also contains the save button.
                Until this button is pressed and an alert appears to confirm the save, anything you have added or modified is NOT saved to the server</p><br/>
            </Container>
        )
    }

}

const Container = styled.div`
  display: flex;
  flex-shrink: 0;
  flex-direction: column;
  height: 100vh;
  width: 84vw;
  margin: 4vh 8vw;
  scroll-behavior: auto;
`;