import React, {Component} from "react";

export default class ErrorPage extends Component{
    config;
    component;
    constructor(props) {
        super(props);
        this.config=props.match.params.config;
        this.component=props.match.params.component;

    }

    state = {
        next_url: '',
        url_text: ''
    }

    componentDidMount() {
        let url = '';
        let text = '';
        switch(this.component){
            case 'viewer':
                url = '/viewer/'+this.config;
                text = 'Go back to the search page and try a different query'
                break;
            case 'search':
                url = '/help'
                text = 'Could find that configuration. Try re-entering URL or view help'
                break;
            case 'designer':
                url = '/help'
                text = 'Could find that configuration or incorrect access code. Try re-entering URL or view help'
                break;
        }

        this.setState(() => ({
            next_url: url,
            url_text: text
        }))
    }

    render(){
        return(
            <div>
                <h2>BrowseInOne</h2>
                <h1>ERROR</h1>
                <p>We're sorry that there's been an error while attempting to access the data you requested.</p>
                <p><a href={this.state.next_url}>{this.state.url_text}</a></p>
            </div>
        )
    }

}