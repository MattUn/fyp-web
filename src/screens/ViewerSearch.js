import React, {Component} from "react";
import styled from "styled-components";
import ViewerSidebar from "../components/viewer/ViewerSidebar";
import FeatherIcon from "react-native-vector-icons/dist/Feather";
import {Link, Redirect} from 'react-router-dom'

export default class ViewerSearch extends Component{
    constructor(props) {
        super(props);
        let config=props.match.params.config;
        this.init(config)
        this.updateWindowDimensions = this.updateWindowDimensions.bind(this);
    }

    componentDidMount() {
        this.updateWindowDimensions();
        window.addEventListener('resize', this.updateWindowDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener('resize', this.updateWindowDimensions);
    }

    updateWindowDimensions() {
        this.setState({ width: window.innerWidth, height: window.innerHeight });
    }

    state = {
        config_id: '',
        name: '',
        description: '',
        question: '',
        image: '',
        width: 0,
        height: 0,
        query: '',
    }

    init = (config_id) => {
        ServerFetchStart(config_id)
            .catch(() => {this.setState({config_id: 'invalid_true'})})
            .then((result) => {
            this.setState(() => ({
                config_id: config_id,
                name: result['name'],
                description: result['description'],
                question: result['question'],
                image: result['image']
            }))
        })
    }

    render(){
        let bgDivStyle = {
            backgroundImage: 'url('+this.state.image+')',
            backgroundSize: 'cover',
            width: this.state.width*0.8,
            height: this.state.height,
            opacity: 0.8,
        }
        let searchButtonStyle = {
            top: 5,
                right: 15,
                position: "absolute",
                color: "rgba(128,128,128,1)",
                fontSize: 40,
                height: 30,
                width: 30
        }
        if (this.state.config_id==='invalid_true') {
            return <Redirect to='/error/search/invalid'/>
        }

        return (
            <Container>
                <ViewerSidebar state={this.state}/>
                <div ref="image-pane" style={bgDivStyle} className="background_main">
                    <CentralLozenge>
                        <Question>{this.state.question}</Question>
                        <SearchTextboxStack>
                            <SearchTextbox placeholder="  Search here" value={this.state.query} onChange={e => this.setState({ query: e.target.value })}/>
                            <Link to={'search/'+this.state.config_id+'/'+encodeURIComponent(this.state.query)}>
                                <FeatherIcon
                                    name="search"
                                    style={searchButtonStyle}
                                />
                            </Link>
                        </SearchTextboxStack>
                    </CentralLozenge>
                </div>
            </Container>
        );
    }
}

function ServerFetchStart(config_code) {
    require('dotenv').config()
    return fetch(process.env.REACT_APP_SERVER_URL+'start/'+config_code,
        {
            method: 'GET',
            headers: {
                accept: 'application/json',
            },
        })
        .then(response => {
            if(response.ok) {
                return response;
            } else {
                throw Error(`Request rejected: status code ${response.status}`);
            }
        })
        .then((response) => response.json())
}

const Container = styled.div`
  display: flex;
  flex-shrink: 0;
  flex-direction: row;
  height: 100vh;
  width: 100vw;
`;

const CentralLozenge = styled.div`
  width: 70%;
  height: 20%;
  background-color: rgba(230,230,230,0.9);
  border-radius: 200px;
  flex-direction: column;
  display: flex;
  margin-top: 40vh;
  margin-left: 15%;
`;

const Question = styled.span`
  font-family: Roboto,sans-serif;
  font-style: normal;
  font-weight: 700;
  color: #121212;
  text-align: center;
  font-size: 30px;
  margin-top: 20px;
`;

const SearchTextboxStack = styled.div`
  width: 80%;
  height: 50px;
  margin-top: 2%;
  margin-left: 10%;
  margin-right: 10%;
  position: relative;
`;

const SearchTextbox = styled.input`
  font-family: Roboto,sans-serif;
  width: 100%;
  height: 100%;
  position: static;
  font-style: normal;
  font-weight: 400;
  color: #121212;
  border-width: 1px;
  border-color: #000000;
  border-radius: 20px;
  font-size: 18px;
  border-style: solid;
  background: transparent;
`;
